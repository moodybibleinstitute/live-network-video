import XCTest

import LiveNetworkVideoTests

var tests = [XCTestCaseEntry]()
tests += LiveNetworkVideoTests.allTests()
XCTMain(tests)
