import XCTest
@testable import LiveNetworkVideo

final class LiveNetworkVideoTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(LiveNetworkVideoTransmitter().text, "Hello, World!")
        XCTAssertEqual(LiveNetworkVideoReciever().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
